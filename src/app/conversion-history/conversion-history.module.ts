import { NgModule } from "@angular/core";

import { ConversionHistoryComponent } from "./conversion-history.component";
import { SharedModule } from "../shared/shared.module";
import { ConversionHistoryRoutingModule } from "./conversion-history-routing.module";

@NgModule({
  imports: [SharedModule, ConversionHistoryRoutingModule],
  declarations: [ConversionHistoryComponent]
})
export class ConversionHistoryModule {}
