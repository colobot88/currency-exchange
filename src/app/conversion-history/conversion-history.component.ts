import { Component, OnInit, ViewChild } from "@angular/core";
import { ConversionResult } from "../core/models/conversionResult";
import { CurrencyService } from "../core/services/currency.service";
import { MatTable } from "@angular/material";
import { Router } from "@angular/router";

@Component({
  selector: "app-conversion-history",
  templateUrl: "./conversion-history.component.html",
  styleUrls: ["./conversion-history.component.scss"]
})
export class ConversionHistoryComponent implements OnInit {
  @ViewChild(MatTable) table: MatTable<any>;
  displayedColumns: string[] = ["date", "amount", "action"];
  dataSource = [];

  constructor(
    private currencyService: CurrencyService,
    private router: Router
  ) {}

  ngOnInit() {
    this.dataSource = this.currencyService.conversionHistory;
  }

  removeCurrencyItem(conversion) {
    this.dataSource = this.currencyService.removeFromConversionHistory(
      conversion
    );
    this.table.renderRows();
  }

  selectConversion(conversion) {
    this.router.navigate(["/currency-converter"], {
      queryParams: { id: conversion.id }
    });
  }
}
