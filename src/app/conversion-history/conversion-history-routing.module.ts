import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ConversionHistoryComponent } from "./conversion-history.component";

const routes: Routes = [
  {
    path: "",
    component: ConversionHistoryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConversionHistoryRoutingModule {}
