import { NgModule } from "@angular/core";
import { Routes, RouterModule, PreloadAllModules } from "@angular/router";

import { AuthGuardService } from "./core/guards/auth-guard.service";
import { LoginComponent } from "./login/login.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "/currency-converter",
    pathMatch: "full"
  },
  {
    path: "conversion-history",
    loadChildren:
      "./conversion-history/conversion-history.module#ConversionHistoryModule",
    canActivate: [AuthGuardService]
  },
  {
    path: "currency-converter",
    loadChildren:
      "./currency-converter/currency-converter.module#CurrencyConverterModule",
    canActivate: [AuthGuardService]
  },
  {
    path: "login",
    component: LoginComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
