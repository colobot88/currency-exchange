import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FlexLayoutModule } from "@angular/flex-layout";

import { MaterialModule } from "./material/material.module";

import { HeaderComponent } from "./header/header.component";
import { RouterModule } from "@angular/router";

import { CurrencyUnitRatePipe } from "./pipes/currency-unit-rate.pipe";
import { CurrencyDatePipe } from "./pipes/currency-date.pipe";
import { CurrencyEventPipe } from "./pipes/currency-event.pipe";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    FlexLayoutModule,
    RouterModule
  ],
  declarations: [
    HeaderComponent,
    CurrencyUnitRatePipe,
    CurrencyDatePipe,
    CurrencyEventPipe
  ],
  providers: [],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    HeaderComponent,
    CurrencyUnitRatePipe,
    CurrencyDatePipe,
    CurrencyEventPipe
  ]
})
export class SharedModule {}
