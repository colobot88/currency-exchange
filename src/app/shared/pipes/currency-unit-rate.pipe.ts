import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "currencyUnitRate" })
export class CurrencyUnitRatePipe implements PipeTransform {
  transform(fromCurrency: any, toCurrency: any, rate: any): string {
    let result =
      "1 " +
      fromCurrency.currency +
      " = " +
      (fromCurrency.rate / toCurrency.rate).toFixed(6) +
      " " +
      toCurrency.currency;
    return result;
  }
}
