import { Pipe, PipeTransform } from "@angular/core";
import * as moment from "moment";

@Pipe({ name: "currencyDate" })
export class CurrencyDatePipe implements PipeTransform {
  transform(value: number): string {
    let result = moment(value).format("DD/MM/YYYY @ HH:mm");
    return result;
  }
}
