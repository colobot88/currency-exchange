import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "currencyEvent" })
export class CurrencyEventPipe implements PipeTransform {
  transform(value: any): string {
    let result =
      "Converted an amount of " +
      value.amount +
      " from " +
      value.from.currency +
      " to " +
      value.to.currency;
    return result;
  }
}
