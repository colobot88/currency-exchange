import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HTTP_INTERCEPTORS
} from "@angular/common/http";
import { Observable, of, throwError } from "rxjs";

import { User } from "../models/user";

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const users: User[] = [
      { username: "user1", password: "pass1", fullName: "John Doe" },
      { username: "user2", password: "pass2", fullName: "Adam Smith" }
    ];

    if (request.url.endsWith("/users/login") && request.method === "POST") {
      const user = users.find(
        x =>
          x.username === request.body.username &&
          x.password === request.body.password
      );
      if (!user) return error("Username or password is incorrect");
      return ok({
        username: user.username,
        fullName: user.fullName,
        token: `fake-jwt-token`
      });
    }

    return next.handle(request);

    // private helper functions
    function ok(body) {
      return of(new HttpResponse({ status: 200, body }));
    }

    function error(message) {
      return throwError({ status: 400, error: { message } });
    }
  }
}

export let fakeBackendProvider = {
  // use fake backend in place of Http service for backend-less development
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true
};
