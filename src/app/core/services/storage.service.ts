import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class StorageService {
  public setItem(key: string, data: string): string {
    localStorage.setItem(key, data);
    return data;
  }

  public getItem(key: string): string {
    const data = localStorage.getItem(key);
    return data;
  }
}
