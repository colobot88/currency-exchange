import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute, Router } from "@angular/router";
import { BehaviorSubject, Observable } from "rxjs";
import { map, tap } from "rxjs/operators";

import { environment } from "../../../environments/environment";
import { User } from "../models/user";
import { Currency } from "../models/currency";
import { ConversionResult } from "../models/conversionResult";
import { StorageService } from "./storage.service";

@Injectable({ providedIn: "root" })
export class CurrencyService {
  currencies: Currency[];
  conversionHistory: ConversionResult[] =
    JSON.parse(this.storageService.getItem("conversionHistory")) || [];

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {}

  fetchExchangeRates(key: string): Observable<any> {
    return this.http
      .get(
        environment.exchangeApiUrl +
          "exchange-rates?key=" +
          environment.exchangeApiKey
      )
      .pipe(
        map((resp: Currency[]) => {
          this.currencies = resp;
          let filteredResult = resp.filter(
            item => item.currency.toLowerCase().indexOf(key) === 0
          );
          return filteredResult;
        })
      );
  }

  fetchExhangeRateHistory(currency: string, startDate: string) {
    return this.http
      .get(
        environment.exchangeApiUrl +
          "exchange-rates/history?key=" +
          environment.exchangeApiKey +
          "&currency=" +
          currency +
          "&start=" +
          startDate +
          "T00:00:00Z"
      )
      .pipe(
        map((resp: Currency[]) => {
          let exchangeHistoryResult = {
            dataSource: resp,
            statistics: this.calculateMinMaxAvg(resp)
          };
          return exchangeHistoryResult;
        })
      );
  }

  calculateMinMaxAvg(exchangeHistory: any[]) {
    let minMaxAvg = [];
    let total = exchangeHistory.reduce((prev, cur) => prev + +cur.rate, 0);
    let max = exchangeHistory.reduce(
      (prev, cur) => Math.max(prev, +cur.rate),
      Number.NEGATIVE_INFINITY
    );
    let min = exchangeHistory.reduce(
      (prev, cur) => Math.min(prev, +cur.rate),
      Number.POSITIVE_INFINITY
    );
    let average = total / exchangeHistory.length;
    minMaxAvg.push({ name: "Lowest", value: min });
    minMaxAvg.push({ name: "Highest", value: max });
    minMaxAvg.push({ name: "Average", value: average });
    return minMaxAvg;
  }

  convertCurrencies(amount, from, to) {
    from = this.currencies.filter(item => item.currency === from.currency)[0];
    to = this.currencies.filter(item => item.currency === to.currency)[0];
    let conversionResult: ConversionResult = {
      id: this.getLastConversionId(),
      amount: amount,
      from: from,
      to: to,
      result: (amount * from.rate) / to.rate,
      date: new Date()
    };

    this.addConversionHistory(conversionResult);

    return conversionResult;
  }

  addConversionHistory(conversion: ConversionResult) {
    this.conversionHistory.push(conversion);
    this.storageService.setItem(
      "conversionHistory",
      JSON.stringify(this.conversionHistory)
    );
  }

  getConversionHistoryById(id: number) {
    return this.conversionHistory.filter(conversion => conversion.id === id)[0];
  }

  getLastConversionId() {
    if (this.conversionHistory.length === 0) {
      return 0;
    } else {
      return this.conversionHistory[this.conversionHistory.length - 1].id + 1;
    }
  }

  removeFromConversionHistory(conversionToDelete: ConversionResult) {
    this.conversionHistory = this.conversionHistory.filter(
      conversion => conversion.id !== conversionToDelete.id
    );
    this.storageService.setItem(
      "conversionHistory",
      JSON.stringify(this.conversionHistory)
    );

    return this.conversionHistory;
  }
}
