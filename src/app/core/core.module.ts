import { CommonModule } from "@angular/common";
import { HttpClientModule } from "@angular/common/http";
import { NgModule, Optional, SkipSelf } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { throwIfAlreadyLoaded } from "./module-import-guard";
import { AuthGuardService } from "./guards/auth-guard.service";
import { AuthService } from "./services/auth.service";
import { CurrencyService } from "./services/currency.service";
import { StorageService } from "./services/storage.service";

@NgModule({
  imports: [CommonModule, BrowserAnimationsModule, HttpClientModule],
  exports: [],
  declarations: [],
  providers: [AuthGuardService, AuthService, CurrencyService, StorageService]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, "CoreModule");
  }
}
