import { Currency } from "./currency";

export interface ConversionResult {
  id: number;
  amount: number;
  from: Currency;
  to: Currency;
  result: number;
  date: Date;
}
