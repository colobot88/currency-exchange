import { Injectable } from "@angular/core";
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";

import { AuthService } from "../services/auth.service";

@Injectable({ providedIn: "root" })
export class AuthGuardService implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.authService.getUser()) {
      // already logged in. then canActivate: true
      return true;
    }

    // not logged in, redirect to login page with return url.
    this.router.navigate(["/login"], { queryParams: { returnUrl: state.url } });
    return false;
  }
}
