import { Component, OnInit } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { AuthService } from "../core/services/auth.service";
import { ActivatedRoute, Router } from "@angular/router";
import { MatSnackBar } from "@angular/material";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  returnUrl: string;
  usernameCtrl = new FormControl("", [Validators.required]);
  passwordCtrl = new FormControl("", [Validators.required]);

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    // reset login status
    this.authService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";
  }

  login() {
    if (this.usernameCtrl.invalid || this.passwordCtrl.invalid) {
      return;
    }
    this.authService
      .login(this.usernameCtrl.value, this.passwordCtrl.value)
      .subscribe(
        data => {
          console.log(data);
          // login successful so redirect to return url
          this.router.navigateByUrl(this.returnUrl);
        },
        error => {
          console.log(error);
          this.snackBar.open(
            "You have entered an invalid username or password!",
            "",
            {
              duration: 2000
              // verticalPosition: "top"
            }
          );
        }
      );
  }
}
