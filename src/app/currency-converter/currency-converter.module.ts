import { NgModule } from "@angular/core";

import { CurrencyConverterComponent } from "./currency-converter.component";
import { ExchangeHistoryComponent } from "./exchange-history/exchange-history.component";
import { SharedModule } from "../shared/shared.module";
import { CurrencyConverterRoutingModule } from "./currency-converter-routing.module";

@NgModule({
  imports: [SharedModule, CurrencyConverterRoutingModule],
  declarations: [CurrencyConverterComponent, ExchangeHistoryComponent]
})
export class CurrencyConverterModule {}
