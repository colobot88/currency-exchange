import { Component } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from "@angular/forms";
import { startWith, debounceTime } from "rxjs/operators";
import { CurrencyService } from "../core/services/currency.service";
import { Currency } from "../core/models/currency";
import { ConversionResult } from "../core/models/conversionResult";
import { ActivatedRoute } from "@angular/router";
import * as moment from "moment";
import { MatTableDataSource } from "@angular/material";

@Component({
  selector: "app-currency-converter",
  templateUrl: "./currency-converter.component.html",
  styleUrls: ["./currency-converter.component.scss"]
})
export class CurrencyConverterComponent {
  exchangeForm: FormGroup;
  // Form Controls
  amountCtrl = new FormControl("", [Validators.required]);
  fromCtrl = new FormControl("", [Validators.required]);
  toCtrl = new FormControl("", [Validators.required]);

  // Form Values
  fromCurrency: Currency;
  toCurrency: Currency;
  conversionResult: ConversionResult;

  // Form items-currencies for autocomplete
  filteredFromCurrencyItems: Currency[] = [];
  filteredToCurrencyItems: Currency[] = [];

  // Exchange History
  exchangeHistory: MatTableDataSource<any>;
  statistics: MatTableDataSource<any>;

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private currencyService: CurrencyService
  ) {}

  ngOnInit() {
    // initial request to get the list of currencies
    this.currencyService.fetchExchangeRates("").subscribe(results => {
      this.filteredFromCurrencyItems = results;
      this.filteredToCurrencyItems = results;
    });

    this.initializeForm();

    this.route.queryParams.subscribe(params => {
      if (params.id) {
        let conversion = this.currencyService.getConversionHistoryById(
          +params.id
        );

        this.conversionResult = conversion;
        this.exchangeForm.get("amountCtrl").setValue(+conversion.amount);
        this.exchangeForm.get("fromCtrl").setValue(conversion.from);
        this.exchangeForm.get("toCtrl").setValue(conversion.to);
      }
    });
  }

  initializeForm() {
    this.exchangeForm = this.fb.group({
      amountCtrl: new FormControl("", [Validators.required]),
      fromCtrl: new FormControl("", [Validators.required]),
      toCtrl: new FormControl("", [Validators.required])
    });
    // get autocomplete result list on value change
    this.exchangeForm
      .get("fromCtrl")
      .valueChanges.pipe(debounceTime(300))
      .subscribe(val => {
        if (val && typeof val !== "object") {
          this.currencyService.fetchExchangeRates(val).subscribe(results => {
            this.filteredFromCurrencyItems = results;
          });
        }
      });

    // get autocomplete result list on value change
    this.exchangeForm
      .get("toCtrl")
      .valueChanges.pipe(
        debounceTime(300),
        startWith(null)
      )
      .subscribe(val => {
        if (val && typeof val !== "object") {
          this.currencyService.fetchExchangeRates(val).subscribe(results => {
            this.filteredToCurrencyItems = results;
          });
        }
      });
  }

  displayFn(item: Currency) {
    if (item) {
      return item.currency;
    }
  }

  onFromCurrencySelected(selected) {
    this.fromCurrency = selected.value;
  }

  onToCurrencySelected(selected) {
    this.toCurrency = selected.value;
  }

  changeFromToValues() {
    let tempFrom = this.exchangeForm.get("fromCtrl").value;
    let tempTo = this.exchangeForm.get("toCtrl").value;
    this.exchangeForm.get("fromCtrl").setValue(tempTo);
    this.exchangeForm.get("toCtrl").setValue(tempFrom);
    tempFrom = this.fromCurrency;
    this.fromCurrency = this.toCurrency;
    this.toCurrency = tempFrom;
    tempFrom = this.filteredFromCurrencyItems;
    this.filteredFromCurrencyItems = this.filteredToCurrencyItems;
    this.filteredToCurrencyItems = this.filteredFromCurrencyItems;
  }

  convertCurrency() {
    if (this.exchangeForm.invalid) {
      return;
    }
    this.currencyService.fetchExchangeRates("").subscribe(results => {
      let result = this.currencyService.convertCurrencies(
        this.exchangeForm.get("amountCtrl").value,
        this.exchangeForm.get("fromCtrl").value,
        this.exchangeForm.get("toCtrl").value
      );
      this.conversionResult = result;
    });

    this.currencyService
      .fetchExhangeRateHistory(
        this.exchangeForm.get("fromCtrl").value.currency,
        moment()
          .subtract(7, "d")
          .format("YYYY-MM-DD")
      )
      .subscribe(exchangeHistory => {
        this.exchangeHistory = new MatTableDataSource(
          exchangeHistory.dataSource
        );
        this.statistics = new MatTableDataSource(exchangeHistory.statistics);
      });
  }

  getFromCurrencyValue() {
    return this.exchangeForm.get("fromCtrl").value.currency;
  }
}
