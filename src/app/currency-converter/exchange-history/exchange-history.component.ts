import { Component, OnInit, ViewChild, Input } from "@angular/core";
import {
  MatTable,
  MatSort,
  MatTableDataSource,
  MatSortable
} from "@angular/material";
import { Router } from "@angular/router";
import { CurrencyService } from "src/app/core/services/currency.service";
import * as moment from "moment";

const durations = [
  { title: "7 days", value: 7 },
  { title: "14 days", value: 14 },
  { title: "30 days", value: 30 }
];

@Component({
  selector: "app-exchange-history",
  templateUrl: "./exchange-history.component.html",
  styleUrls: ["./exchange-history.component.scss"]
})
export class ExchangeHistoryComponent implements OnInit {
  @Input() currency;
  @Input() exchangeHistory: MatTableDataSource<any>;
  @Input() statistics: MatTableDataSource<any>;
  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild(MatSort) sort: MatSort;
  displayedColumns: string[] = ["timestamp", "rate"];
  displayedStatsColumns: string[] = ["name", "value"];
  durations: any[];
  duration: any;

  constructor(private currencyService: CurrencyService) {}

  ngOnInit() {
    this.durations = durations;
    this.duration = this.durations[0].value;
  }

  ngAfterViewInit(): void {
    this.exchangeHistory.sort = this.sort;
  }

  onDurationChange(selectedDuration) {
    this.currencyService
      .fetchExhangeRateHistory(
        this.currency,
        moment()
          .subtract(selectedDuration.value, "d")
          .format("YYYY-MM-DD")
      )
      .subscribe(exchangeHistory => {
        this.exchangeHistory = new MatTableDataSource(
          exchangeHistory.dataSource
        );
        this.exchangeHistory.sort = this.sort;
        this.statistics = new MatTableDataSource(exchangeHistory.statistics);
      });
  }
}
